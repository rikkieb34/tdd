import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Url {
    private final String protocol;
    private final String domain;
    private final String path;
    private boolean containsSubDomain = false;

    public Url(String url) {
        this.protocol = this.setProtocol(url);
        this.domain = this.setDomain(url);
        this.path = this.setPath(url);
    }

    private String setProtocol(String url) {
        String[] splitUrl = url.split(":");
        String protocol = splitUrl[0];
        Pattern pattern = Pattern.compile("^http|https|ftp|file");
        Matcher matcher = pattern.matcher(protocol.toLowerCase());
        if(!matcher.find()) {
            throw new IllegalArgumentException("Not a valid protocol");
        }
        return protocol.toLowerCase();
    }

    public String getProtocol() {
        return this.protocol;
    }

    private String setDomain(String url) {
        String[] splitUrl = url.split("://");
        String domain = "";

        if(splitUrl.length <= 1) {
            throw new IllegalArgumentException("Domain cannot be empty");
        }

        if(splitUrl[1].indexOf('/') != -1) {
            domain = splitUrl[1].substring(0, splitUrl[1].indexOf('/'));
        } else {
            domain = splitUrl[1];
        }

        if(domain.contains("www.")) {
            this.containsSubDomain = true;
            domain = domain.substring(4);
        }

        Pattern pattern = Pattern.compile("[^A-Za-z0-9._\\-]");
        Matcher matcher = pattern.matcher(domain);
        if(matcher.find()) {
            throw new IllegalArgumentException("Special characters are not allowed in the domain");
        }

        return domain.toLowerCase();
    }

    public String getDomain() {
        return this.domain;
    }

    private String setPath(String url) {
        String[] splitUrl = url.split("://");
        String path = "";
        if(splitUrl[1].indexOf('/') != -1) {
            path = splitUrl[1].substring(splitUrl[1].indexOf('/') + 1);
        }
        return path.toLowerCase();
    }

    public String getPath() {
        return this.path;
    }

    public String toString() {
        String subDomain = "";
        if(this.containsSubDomain) {
            subDomain = "www.";
        }
        return this.protocol + "://" + subDomain + this.domain + "/" + this.path;
    }
}
